import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:tasks_app/controller/helper.dart';
import 'package:tasks_app/methods/tasks.dart';

class FireStoreController {
  FireStoreController._();
BuildContext? context;
  static FireStoreController _instance = FireStoreController._();
  FirebaseFirestore _FireStore = FirebaseFirestore.instance;

  static FireStoreController get instance {
    return _instance;
  }
  FireStoreController setContext(BuildContext context){
    this.context=context;
    return this;
}
//DataBase(CRUD)
//Creat
//Read
//Update
//Delet
  Future create(Task task) async {
    DocumentReference documentReference =
        await _FireStore.collection('Tasks').add(task.toMap());
    // print('id : ${documentReference.id}');
    return documentReference;
  }
  Future delet(Task task) async {
    try {
      await _FireStore.collection('Tasks').doc(task.id).delete();
    }
    on  FirebaseException catch(e){
      Helper.showMessage(context!, "field deleted",error: true);
    }
    catch (e) {
      print("Exception${e}");
    }

  }
  Future delete(String id)async{
    await _FireStore.collection('Tasks').doc(id).delete();

  }
  Future<List<Task>> read() async {
    QuerySnapshot querySnapshot = await _FireStore.collection('Tasks').get();
    List<Task> tasks = [];
    querySnapshot.docs.forEach((element) {
      tasks.add(Task(
          title: element['tittle'],
          //element.data()['title'] the old version flutter,
          details: element['details'],
          id: element.id));
    });
    return tasks;
  }
  Future update(Task task) async {

    try {
      await _FireStore.collection('Tasks').doc(task.id).update(task.toMap());
    }
    on  FirebaseException catch(e){
      print("FirebaseException: $e");
    }
    catch (e) {
      print("Exception${e}");
    }

  }
  Stream <QuerySnapshot> readStream() async*{
    yield* _FireStore.collection('Tasks').snapshots();
    //تدفق مستمر انبعاث
    //yield atirable the last atireble
  }
//FireStore Stream
}
