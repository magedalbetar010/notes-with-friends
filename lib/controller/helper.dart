import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Helper {
  static void showMessage(BuildContext context, String message,
      {bool error = false}) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(message),
        margin:EdgeInsets.all(10) ,
        behavior: SnackBarBehavior.floating,
        backgroundColor:error?Colors.red:Colors.green ,
        duration:Duration(seconds: 2) ,
      ),
    );
  }
}
