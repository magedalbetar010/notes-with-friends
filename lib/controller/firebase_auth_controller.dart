import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:tasks_app/controller/helper.dart';

class FirebaseAuthController {
  FirebaseAuthController._();
BuildContext? context;
  static FirebaseAuthController _instance = FirebaseAuthController._();

  static FirebaseAuthController get instance {
    return _instance;
  }
  FirebaseAuthController setContext(BuildContext context){
    this.context = context;
    return this;
  }

  FirebaseAuth _auth = FirebaseAuth.instance;

  Future<UserCredential?> singIn(String email, String password) async {
    try {
      UserCredential sing = await _auth.signInWithEmailAndPassword(
          email: email, password: password);
      return sing;
    } on FirebaseAuthException catch (e) {

      if (e.code == "invalid-email") {
        print("exception with ${e.code} :: Message ${e.message}");
        if(context!=null) Helper.showMessage(context!, '${e.message}');

      } else if (e.code == "user-disabled") {
        print("exception with ${e.code} :: Message ${e.message}");
        if(context!=null) Helper.showMessage(context!, 'user disabled',error: true);

      } else if (e.code == "user-not-found") {
        print("exception with ${e.code} :: Message ${e.message}");
        if(context!=null) Helper.showMessage(context!, 'user not found',error: true);

      } else if (e.code == "wrong-password") {
        // print("exception with ${e.code} :: Message ${e.message}",);
        if(context!=null) Helper.showMessage(context!, 'Wrong password',error: true);

      }
    } catch (e) {
      print("exception with $e");
      if(context!=null) Helper.showMessage(context!, 'it is wrong',error: true);

    }
    return null;
  }
  Future logOut()async{
    await _auth.signOut();
    if(context!=null) Helper.showMessage(context!, 'success');

  }
  Future<UserCredential?> createAccount(String email, String password) async {
    try {
      UserCredential create = await _auth.createUserWithEmailAndPassword(
          email: email, password: password);
      return create;
    } on FirebaseAuthException catch (e) {
      if (e.code == "email-already-in-use") {
        if(context!=null) Helper.showMessage(context!, 'the Email already in use',error: true);
      } else if (e.code == "invalid-email") {
        if(context!=null) Helper.showMessage(context!, 'invalid email',error: true);

      } else if (e.code == "operation-not-allowed") {
        if(context!=null) Helper.showMessage(context!, 'operation not allowed',error: true);

      } else if (e.code == "weak-password") {

        if(context!=null) Helper.showMessage(context!, 'pleas enter weak password',error: true);

      }
    } catch (e) {
      // print("exception with $e");
    }
    return null;
  }
  bool isLogedIN ()=>_auth.currentUser!=null;
}
