import 'package:flutter/material.dart';
import 'package:tasks_app/controller/fire_store_controller.dart';
import 'package:tasks_app/methods/tasks.dart';

class UpdateScreen extends StatefulWidget {
  String? name;
  String? details;
  String? id;

  UpdateScreen({Key? key, this.name, this.details, this.id}) : super(key: key);

  @override
  _UpdateScreenState createState() => _UpdateScreenState();
}

class _UpdateScreenState extends State<UpdateScreen> {
  late TextEditingController _nameTextController;
  late TextEditingController _detialsTextController;

  @override
  void initState() {
    _nameTextController = TextEditingController(text: widget.name);
    _detialsTextController = TextEditingController(text: widget.details);

    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue,
        title: const Text("Update Note"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(12.0),
        child: Column(
          children: [
            TextField(
              keyboardType: TextInputType.name,
              controller: _nameTextController,
              decoration: const InputDecoration(
                hintText: 'tittle',
              ),
            ),
            const SizedBox(
              height: 15,
            ),
            TextField(
              maxLines: 3,
              controller: _detialsTextController,
              keyboardType: TextInputType.name,
              decoration: const InputDecoration(hintText: 'details'),
            ),
            const SizedBox(
              height: 10,
            ),
            SizedBox(
              width: double.infinity,
              height: 45,
              child: ElevatedButton.icon(
                style: ElevatedButton.styleFrom(
                    shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20),
                )),
                icon: const Icon(Icons.save),
                onPressed: () async {
                   await update();
                },
                label: const Text('Update'),
              ),
            ),
          ],
        ),
      ),
    );
  }

  // Future update(int index) async {
  //   await FireStoreController.instance.update(_notes.elementAt(index));
  // }
  Task getTask() {
    return Task(
        id: widget.id!,
        title: _nameTextController.text,
        details: _detialsTextController.text);
  }
  Future update()async{
    bool saved = await FireStoreController.instance.update(getTask());
    if(saved){
      print("it is Updated");
    }else{
      print("the wrong to update");
    }
  }
}
