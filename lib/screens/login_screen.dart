import 'dart:math';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:tasks_app/controller/firebase_auth_controller.dart';
import 'package:tasks_app/controller/helper.dart';


class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  bool pass = true;
  TextEditingController? _emailTextEditingController;
  TextEditingController? _passwordTextEditingController;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _emailTextEditingController = TextEditingController();
    _passwordTextEditingController = TextEditingController();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _emailTextEditingController?.dispose();
    _passwordTextEditingController?.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        margin: EdgeInsets.zero,
        width: double.infinity,
        height: double.infinity,
        // padding: EdgeInsets.all(12),
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage("images/note.png"),
            fit: BoxFit.cover,
          ),
        ),
        child: Container(
          height: double.infinity,
          width: double.infinity,
          decoration: BoxDecoration(
            color: Colors.white.withOpacity(0.7),
          ),
          child: SafeArea(
            child: SingleChildScrollView(

              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  // const Spacer(),
                  SizedBox(height: 120,),
                  Text(
                    "Sing In",
                    style: GoogleFonts.nunito(
                      fontSize: 30,
                      color: const Color(0xff23203F),
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Text(
                    "Login to start using app,",
                    style: GoogleFonts.roboto(
                      fontSize: 18,
                      color: const Color(0xff716F87),
                      fontWeight: FontWeight.bold,
                    ),
                  ),

                  const SizedBox(
                    height: 50,
                  ),
                  Container(
                    margin: const EdgeInsets.all(20),
                    height: 180,
                    width: double.infinity,
                    decoration:  BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(20),
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        TextField(
                          controller: _emailTextEditingController,
                          keyboardType: TextInputType.emailAddress,
                          decoration: InputDecoration(
                            hintText: 'EmailAddress',
                            hintStyle: GoogleFonts.roboto(
                              color: const Color(0xff716F87),
                              fontSize: 22,
                            ),
                            // TextStyle(
                            //     color: Color.fromARGB(255, 117, 117, 117),
                            //     fontSize: 18,
                            //     fontFamily: 'Oswald'),
                            prefixIcon: const Icon(
                              Icons.email,
                              color: Colors.black,
                            ),
                          ),
                        ),
                        TextField(
                          controller: _passwordTextEditingController,
                          decoration: InputDecoration(
                              hintText: 'Password',
                              hintStyle: GoogleFonts.roboto(
                                color: const Color(0xff716F87),
                                fontSize: 22,
                              ),
                              prefixIcon: const Icon(
                                Icons.lock,
                                color: Colors.black,
                              ),
                              suffix: IconButton(
                                onPressed: () {
                                  setState(() {
                                    pass = !pass;
                                  });
                                },
                                icon: pass
                                    ? const Icon(
                                        Icons.visibility_off,
                                        color: Colors.black,
                                      )
                                    : const Icon(
                                        Icons.remove_red_eye_rounded,
                                        color: Colors.black,
                                      ),
                              )),

                          style: const TextStyle(color: Colors.black),
                          keyboardType: TextInputType.text,
                          // obscureText: password(pass),
                          obscureText: pass,
                        ),
                      ],
                    ),
                  ),

                  const SizedBox(
                    height: 20,
                  ),
                  Center(
                    child: SizedBox(
                      // width: MediaQuery.of(context).size.width*0.116,
                      // height: MediaQuery.of(context).size.height*0.70,
                      height: 53,
                      width: 315,
                      child: ElevatedButton(
                        onPressed: () {
                          loginScreen();
                        },
                        style: ElevatedButton.styleFrom(
                          primary: const Color(0XFF6A90F2),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30),
                          ),
                        ),
                        child: const SizedBox(
                            width: double.infinity,
                            child: Text(
                              'Login',
                              textAlign: TextAlign.center,
                            )),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "Don’t have an account?",
                        style: GoogleFonts.roboto(
                          fontSize: 18,
                          color: const Color(0xff9391A4),
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                      TextButton(
                        onPressed: () {
                          setState(() {
                            Navigator.pushReplacementNamed(
                                context, '/sing_up_screen');
                          });
                        },
                        child: Text(
                          "Sign up",
                          style: GoogleFonts.roboto(
                            fontSize: 18,
                            color: Color(0xff23203F),
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      )
                    ],
                  ),
                  // SizedBox(
                  //   height: 50,
                  //   child: ElevatedButton(
                  //     onPressed: () async {
                  //       await createAccount();
                  //       // _emailTextEditingController;
                  //       // _emailTextEditingController;
                  //     },
                  //     style: ElevatedButton.styleFrom(
                  //       primary: const Color.fromARGB(255, 251, 192, 45),
                  //       shape: RoundedRectangleBorder(
                  //         borderRadius: BorderRadius.circular(10),
                  //
                  //         // side: BorderSide(
                  //         //   width: 1,
                  //         //   color: Colors.red.shade900,
                  //         // )
                  //       ),
                  //     ),
                  //     child: const SizedBox(
                  //         width: double.infinity,
                  //         child: Text(
                  //           'Create Account',
                  //           // colortext: Color.fromARGB(255, 33, 33, 33),
                  //           textAlign: TextAlign.center,
                  //         )),
                  //   ),
                  // ),
                  // const Spacer()
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void loginScreen() {
    if (_emailTextEditingController!.text.isNotEmpty &&
        _passwordTextEditingController!.text.isNotEmpty) {
      login();
    }
  }

  Future login() async {
    UserCredential? userCredential = await FirebaseAuthController.instance
        .setContext(context)
        .singIn(_emailTextEditingController!.text,
            _passwordTextEditingController!.text);
    if (userCredential != null) {
      Helper.showMessage(context, "You are logged in successfully");
      Navigator.pushReplacementNamed(context, '/home_screen');
    }
  }

}
