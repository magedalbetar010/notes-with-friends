import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../controller/firebase_auth_controller.dart';

class SingInScreen extends StatefulWidget {
  const SingInScreen({Key? key}) : super(key: key);

  @override
  State<SingInScreen> createState() => _SingInScreenState();
}

class _SingInScreenState extends State<SingInScreen> {
  TextEditingController? _emailController;
  TextEditingController? _passController;
  TextEditingController? _nameController;
  TextEditingController? _lastNameController;
  TextEditingController? _phoneController;

  bool pass = true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _emailController = TextEditingController();
    _passController = TextEditingController();
    _nameController = TextEditingController();
    _lastNameController = TextEditingController();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(

          backgroundColor: Colors.transparent,
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back_ios,
              color: Colors.black,
            ),
            onPressed: () {
              Navigator.pushReplacementNamed(context, '/login_screen');
            },
          ),
          elevation: 0.0,),
          // flexibleSpace: Stack(
          //   children: [
          //     Image(
          //       image: AssetImage("images/note.png"),
          //       height: double.infinity,
          //       width: double.infinity,
          //       fit: BoxFit.cover,
          //     ),
          //     Container(
          //         height: double.infinity,
          //         width: double.infinity,
          //         decoration: BoxDecoration(
          //           color: Colors.white.withOpacity(0.7),
          //         )),
          //
          //   ],
          // )),
      body: Container(
        /*padding: EdgeInsets.zero,*/
        margin: EdgeInsets.zero,
        width: double.infinity,
        height: double.infinity,
        // padding: EdgeInsets.all(12),
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("images/note.png"),
            fit: BoxFit.fill,
          ),
        ),
        child: Container(
          height: double.infinity,
          width: double.infinity,
          decoration: BoxDecoration(
            color: Colors.white.withOpacity(0.7),
          ),
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SafeArea(
                  child: Text(
                    "Sing Up",
                    style: GoogleFonts.nunito(
                      fontSize: 30,
                      color: const Color(0xff23203F),
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  "Create an account",
                  style: GoogleFonts.roboto(
                    fontSize: 18,
                    color: const Color(0xff9391A4),
                    fontWeight: FontWeight.w400,
                  ),
                ),
                SizedBox(
                  height: 52,
                ),
                Container(
                  height: 330,
                  width: double.infinity,
                  padding: EdgeInsets.all(13),
                  margin: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: Colors.white,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      TextField(
                        controller: _nameController,
                        keyboardType: TextInputType.emailAddress,
                        decoration: InputDecoration(
                          hintText: 'Name',
                          hintStyle: GoogleFonts.roboto(
                            color: const Color(0xff716F87),
                            fontSize: 22,
                          ),
                          // TextStyle(
                          //     color: Color.fromARGB(255, 117, 117, 117),
                          //     fontSize: 18,
                          //     fontFamily: 'Oswald'),
                          prefixIcon: const Icon(
                            Icons.account_circle_sharp,
                            color: Colors.black,
                          ),
                        ),
                      ),
                      TextField(
                        controller: _lastNameController,
                        keyboardType: TextInputType.emailAddress,
                        decoration: InputDecoration(
                          hintText: 'Last Name',
                          hintStyle: GoogleFonts.roboto(
                            color: const Color(0xff716F87),
                            fontSize: 22,
                          ),
                          // TextStyle(
                          //     color: Color.fromARGB(255, 117, 117, 117),
                          //     fontSize: 18,
                          //     fontFamily: 'Oswald'),
                          prefixIcon: const Icon(
                            Icons.info,
                            color: Colors.black,
                          ),
                        ),
                      ),
                      TextField(
                        controller: _emailController,
                        keyboardType: TextInputType.emailAddress,
                        decoration: InputDecoration(
                          hintText: 'EmailAddress',
                          hintStyle: GoogleFonts.roboto(
                            color: const Color(0xff716F87),
                            fontSize: 22,
                          ),
                          // TextStyle(
                          //     color: Color.fromARGB(255, 117, 117, 117),
                          //     fontSize: 18,
                          //     fontFamily: 'Oswald'),
                          prefixIcon: const Icon(
                            Icons.email,
                            color: Colors.black,
                          ),
                        ),
                      ),
                      TextField(
                        controller: _phoneController,
                        keyboardType: TextInputType.phone,
                        decoration: InputDecoration(
                          hintText: 'Phone',
                          hintStyle: GoogleFonts.roboto(
                            color: const Color(0xff716F87),
                            fontSize: 22,
                          ),
                          prefixIcon: const Icon(
                            Icons.phone,
                            color: Colors.black,
                          ),
                        ),
                      ),
                      TextField(
                        controller: _passController,
                        decoration: InputDecoration(
                            hintText: 'Password',
                            hintStyle: GoogleFonts.roboto(
                              color: const Color(0xff716F87),
                              fontSize: 22,
                            ),
                            prefixIcon: const Icon(
                              Icons.lock,
                              color: Colors.black,
                            ),
                            suffix: IconButton(
                              onPressed: () {
                                setState(() {
                                  pass = !pass;
                                });
                              },
                              icon: pass
                                  ? const Icon(
                                      Icons.visibility_off,
                                      color: Colors.black,
                                    )
                                  : const Icon(
                                      Icons.remove_red_eye_rounded,
                                      color: Colors.black,
                                    ),
                            )),

                        style: const TextStyle(color: Colors.black),
                        keyboardType: TextInputType.text,
                        // obscureText: password(pass),
                        obscureText: pass,
                      ),
                    ],
                  ),
                ),
                // Spacer(),
                SizedBox(
                  height: 70,
                ),
                SizedBox(
                  // width: MediaQuery.of(context).size.width*0.116,
                  // height: MediaQuery.of(context).size.height*0.70,
                  height: 53,
                  width: 315,
                  child: ElevatedButton(
                    onPressed: () async {
                      await createAccount();
                    },
                    style: ElevatedButton.styleFrom(
                      primary: const Color(0XFF6A90F2),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30),
                      ),
                    ),
                    child: SizedBox(
                      width: double.infinity,
                      child: Text(
                        'Sing Up',
                        textAlign: TextAlign.center,
                        style: GoogleFonts.roboto(
                          color: Colors.white,
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                        ),
                        // style: GoogleFonts.roboto()
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        // ],
        // ),
      ),
    );
  }

  Future createAccount() async {
    UserCredential? userCredential = await FirebaseAuthController.instance
        .setContext(context)
        .createAccount(_emailController!.text, _passController!.text);
    if (userCredential != null) {
      Navigator.pushReplacementNamed(context, '/home_screen');
    }
  }
}
/*


 */
