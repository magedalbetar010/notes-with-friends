import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:tasks_app/controller/fire_store_controller.dart';
import 'package:tasks_app/controller/helper.dart';
import 'package:tasks_app/methods/tasks.dart';
import 'package:tasks_app/screens/update_screen.dart';

import '../controller/firebase_auth_controller.dart';
import 'create_note_screen.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  List<Task> _notes = [];
  late Future<List<Task>> _futureNote;

  // late var snapshot;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _futureNote = FireStoreController.instance.read();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue,
        title: const Text("Notes"),
        leading: IconButton(
          onPressed: () async {
            await signOut(context);
          },
          icon: Icon(Icons.logout),
        ),
        actions: [
          // IconButton(
          //     onPressed: () {
          //     },
          //     icon: const Icon(Icons.add))
        ],
      ),
      body: StreamBuilder<QuerySnapshot>(
          stream: FireStoreController.instance.readStream(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              List<QueryDocumentSnapshot> docs = snapshot.data!.docs;
              return ListView.builder(
                itemCount: docs.length,
                itemBuilder: (context, index) {
                  return Card(
                    child: ListTile(
                      trailing: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          IconButton(
                            onPressed: () async {
                              await delete(docs.elementAt(index).id, context);
                              Helper.showMessage(context, 'it is deleted',
                                  error: false); // _notes.removeAt(index);

                            },
                            icon: Icon(Icons.delete),
                            color: Colors.red,
                          ),
                          Container(
                            height: MediaQuery.of(context).size.height,
                            // width:  MediaQuery.of(context).size.width,
                            color: Colors.blue,
                            child: IconButton(
                              onPressed: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => UpdateScreen(
                                            details: docs[index]['details'],
                                            name: docs[index]['tittle'],
                                            id: docs[index].id,
                                          )
                                      //todo: todos[index]
                                      ),
                                );
                              },
                              icon: Icon(Icons.edit),
                              color: Colors.white,
                            ),
                          ),
                        ],
                      ),
                      title: Text(docs.elementAt(index)['tittle']),
                      subtitle: Text(docs.elementAt(index)['details']),
                    ),
                  );
                },
              );
            } else {
              return Center(
                child: CircularProgressIndicator(
                  color: Colors.blue,
                ),
              );
            }
          }),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => const CreateNoteScreen()),
          );
        },
        child: Icon(Icons.add),
      ),
    );
  }

  Future delete(String id, BuildContext context) async {
    await FireStoreController.instance.setContext(context).delete(id);

  }

  Future update(int index) async {
    await FireStoreController.instance.update(_notes.elementAt(index));
  }

  Future signOut(context) async {
    await FirebaseAuthController.instance.setContext(context).logOut();
    Navigator.pushReplacementNamed(context, '/login_screen');
  }
}
