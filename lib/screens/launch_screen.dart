import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:tasks_app/controller/firebase_auth_controller.dart';

class LaunchScreen extends StatefulWidget {
  const LaunchScreen({Key? key}) : super(key: key);

  @override
  _LaunchScreenState createState() => _LaunchScreenState();
}

class _LaunchScreenState extends State<LaunchScreen> {
  @override
  void initState() {
    Firebase.initializeApp();
    // TODO: implement initState
    super.initState();
    // String routeName = FirebaseAuthController.instance.isLogedIN()
    //     ? '/home_screen'
    //     : '/login_screen';
    Future.delayed(
      Duration(seconds: 3),
      () {
        Navigator.pushReplacementNamed(context, '/login_screen');
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //0xFF
      backgroundColor: const Color(0xFFFFFFFF),
      body: Container(
        margin: EdgeInsets.zero,
        width: double.infinity,
        height: double.infinity,
        // padding: EdgeInsets.all(12),
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage("images/note.png"),
            fit: BoxFit.cover,
          ),
        ),
        child: Container(
          height: double.infinity,
          width: double.infinity,
          decoration: BoxDecoration(
            color: Colors.white.withOpacity(0.7),
          ),
          child: Center(
            child: Image.asset("images/icone_note.png"),
          ),
        ),
      ),
    );
  }

  isLogedIn() {
    FirebaseAuthController.instance.isLogedIN();
  }
}
