import 'package:flutter/material.dart';
import 'package:tasks_app/controller/fire_store_controller.dart';
import 'package:tasks_app/controller/helper.dart';
import 'package:tasks_app/methods/tasks.dart';

class CreateNoteScreen extends StatefulWidget {
  const CreateNoteScreen({Key? key}) : super(key: key);

  @override
  _CreateNoteScreenState createState() => _CreateNoteScreenState();
}

class _CreateNoteScreenState extends State<CreateNoteScreen> {
  late TextEditingController _nameTextController;
  late TextEditingController _detialsTextController;

  @override
  void initState() {
    _nameTextController = TextEditingController();
    _detialsTextController = TextEditingController();

    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue,
        title: const Text("Create Note"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(12.0),
        child: Column(
          children: [
            TextField(
              keyboardType: TextInputType.name,
              controller: _nameTextController,
              decoration: const InputDecoration(
                hintText: 'tittle',
              ),
            ),
            const SizedBox(
              height: 15,
            ),
            TextField(
              maxLines: 3,
              controller: _detialsTextController,
              keyboardType: TextInputType.name,
              decoration: const InputDecoration(hintText: 'details'),
            ),
            const SizedBox(
              height: 10,
            ),
            SizedBox(
              width: double.infinity,
              height: 45,
              child: ElevatedButton.icon(
                style: ElevatedButton.styleFrom(
                    shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20),
                )),
                icon: const Icon(Icons.save),
                onPressed: () async {
                  Create(context);
                  setState(() {
                    clear();
                  });
                },
                label: const Text('save'),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future Create(context) async {
    if(isRegiht())
  {
  bool created =  await FireStoreController.instance.create(getTask());
  if(created){
    Helper.showMessage(context, 'the note is created',error: false);
  }

  }
  }
  Task getTask() {
    return Task(
        id: "",
        title: _nameTextController.text,
        details: _detialsTextController.text);
  }

  void clear() {
    _nameTextController.text = "";
    _detialsTextController.text = "";
  }

  bool isRegiht() {
    if(_nameTextController.text.isNotEmpty&&_detialsTextController.text.isNotEmpty)
{
  return true;
}else{
      return false;
    }
  }
}
