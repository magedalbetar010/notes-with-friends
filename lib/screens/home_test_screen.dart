// import 'package:flutter/material.dart';
// import 'package:tasks_app/controller/fire_store_controller.dart';
// import 'package:tasks_app/methods/tasks.dart';
// import 'package:tasks_app/screens/update_screen.dart';
//
// import 'create_note_screen.dart';
//
// class NotsScreen extends StatefulWidget {
//   const NotsScreen({Key? key}) : super(key: key);
//
//   @override
//   _NotsScreenState createState() => _NotsScreenState();
// }
//
// class _NotsScreenState extends State<NotsScreen> {
//   List<Task> _notes = [];
//   late Future<List<Task>> _futureNote;
//
//   // late var snapshot;
//
//   @override
//   void initState() {
//     // TODO: implement initState
//     super.initState();
//     _futureNote = FireStoreController.instance.read();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         backgroundColor: Colors.blue,
//         title: const Text("Notes"),
//         actions: [
//           // IconButton(
//           //     onPressed: () {
//           //     },
//           //     icon: const Icon(Icons.add))
//         ],
//       ),
//       body: RefreshIndicator(
//         onRefresh: () => FireStoreController.instance.read(),
//         child: FutureBuilder<List<Task>>(
//             future: _futureNote,
//             builder: (context, snapshot) {
//               {
//                 if (snapshot.connectionState == ConnectionState.waiting) {
//                   return const Center(
//                     child: CircularProgressIndicator(
//                       backgroundColor: Colors.grey,
//                     ),
//                   );
//                 } else if (snapshot.hasError) {
//                   return const Center(
//                     child: Text("error"),
//                   );
//                 } else {
//
//                   _notes = snapshot.data!;
//                   // print(snapshot.data == null ? 'sucsess' : 'failed');
//                   return ListView.builder(
//                     itemCount: _notes.length,
//                     itemBuilder: (context, index) {
//                       return Card(
//                         child: ListTile(
//                           // leading: IconButton(
//                           //   visualDensity: VisualDensity.compact,
//                           //   onPressed: () {
//                           //     Navigator.push(
//                           //       context,
//                           //       MaterialPageRoute(
//                           //           builder: (context) => UpdateScreen(
//                           //                 details: _notes[index].details,
//                           //                 name: _notes[index].title,
//                           //                 id: _notes[index].id,
//                           //               )
//                           //           //todo: todos[index]
//                           //           ),
//                           //     );
//                           //   },
//                           //   icon: Icon(Icons.update),
//                           //   color: Colors.red,
//                           // ),
//                           trailing: Row(
//                             mainAxisSize: MainAxisSize.min,
//                             children: [
//                               IconButton(
//                                 onPressed: () async {
//                                   await delet(index);
//                                 },
//                                 icon: Icon(Icons.delete),
//                                 color: Colors.red,
//                               ),
//                               Container(
//                                 height: MediaQuery.of(context).size.height,
//                                 // width:  MediaQuery.of(context).size.width,
//                                 color: Colors.blue,
//                                 child: IconButton(
//                                   onPressed: () {
//                                     Navigator.push(
//                                       context,
//                                       MaterialPageRoute(
//                                           builder: (context) => UpdateScreen(
//                                             details: _notes[index].details,
//                                             name: _notes[index].title,
//                                             id: _notes[index].id,
//                                           )
//                                         //todo: todos[index]
//                                       ),
//                                     );
//                                   },
//                                   icon: Icon(Icons.edit),
//                                   color: Colors.white,
//                                 ),
//                               ),
//
//                             ],
//                           ),
//                           title: Text(_notes.elementAt(index).title),
//                           subtitle: Text(_notes.elementAt(index).details),
//                         ),
//                       );
//                     },
//                   );
//                 }
//               }
//             }),
//       ),
//       floatingActionButton: FloatingActionButton(
//         onPressed: (){
//           Navigator.push(
//             context,
//             MaterialPageRoute(
//                 builder: (context) => const CreateNoteScreen()),
//           );
//
//         },
//         child: Icon(Icons.add),
//       ),
//     );
//   }
//
//   Future delet(int index) async {
//     bool deleted =
//         await FireStoreController.instance.delet(_notes.elementAt(index));
//     if (deleted) {
//       _notes.removeAt(index);
//     }
//   }
//
//   Future update(int index) async {
//     await FireStoreController.instance.update(_notes.elementAt(index));
//   }
//
// }
