class Task {
  late String id;
  late String title;
  late String details;

  Task({required this.id, required this.title, required this.details});

  Task.fromMap(Map<String, dynamic> map) {
    this.id = map['id'];
    this.title = map['tittle'];
    this.details = map['detials'];
  }

  Map<String, dynamic> toMap() {
    Map<String,dynamic> map = Map<String,dynamic>();
    map['tittle']=this.title;
    map['details']=this.details;
    return map;
  }
}
