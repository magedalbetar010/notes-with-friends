import 'package:flutter/material.dart';
import 'package:tasks_app/screens/create_note_screen.dart';
import 'package:tasks_app/screens/home_screen.dart';
import 'package:tasks_app/screens/launch_screen.dart';
import 'package:tasks_app/screens/login_screen.dart';
import 'package:tasks_app/screens/sing_up_screen.dart';
import 'package:tasks_app/screens/update_screen.dart';

void main() {
  runApp(

      MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(),
      home: const LaunchScreen(),
      routes: {
        '/launch_screen': (context) =>  LaunchScreen(),
        '/login_screen': (context) => const LoginScreen(),
        '/create_note_screen': (context) => const CreateNoteScreen(),
        '/update_screen': (context) => UpdateScreen(),
        '/home_screen': (context) => const HomeScreen(),
        '/sing_up_screen': (context) => const SingInScreen()
      },
    );
  }
}
