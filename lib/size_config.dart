import 'package:flutter/material.dart';
class SizeConfig {
  static late double _screenWidth;
  static late double _screenHeight;
  static late double _blockWidth = 0;
  static late double _blockHeight = 0;
  static late double textMultiplier;
  static late double imageSizeMultiplier;
  static late double heightMultiplier;
  static late double widthMultiplier;
  static late double deviceRatio;
  static late bool isPortrait = true;
  static late bool isMobilePortrait = false;

  static MediaQueryData? _mediaQueryData;

//  void init(BoxConstraints constraints, Orientation orientation) {
//    if (orientation == Orientation.portrait) {
//      _screenWidth = constraints.maxWidth;
//      _screenHeight = constraints.maxHeight;
//      isPortrait = true;
//      if (_screenWidth < 450) {
//        isMobilePortrait = true;
//      }
//    } else {
//      _screenWidth = constraints.maxHeight;
//      _screenHeight = constraints.maxWidth;
//      isPortrait = false;
//      isMobilePortrait = false;
//    }

  void init(BuildContext context) {

    _mediaQueryData = MediaQuery.of(context);
    _screenWidth =  _mediaQueryData!.size.width;
    _screenHeight = _mediaQueryData!.size.height;

    _blockWidth = _screenWidth / 100;
    _blockHeight = _screenHeight / 100;

    textMultiplier = _blockHeight;
    imageSizeMultiplier = _blockWidth;
    heightMultiplier = _blockHeight;
    widthMultiplier = _blockWidth;
    deviceRatio = _screenWidth / _screenHeight;

//    print("---");
//    print("textMultiplier: $textMultiplier");
//    print("heightMultiplier: $heightMultiplier");
//    print("widthMultiplier: $widthMultiplier");
//    print("_screenWidth: $_screenWidth");
//    print("---");
  }

  static double scaleTextFont(double fontSize) {
    double scale = fontSize / 8.96;
    return (textMultiplier * scale);
  }

  static double scaleWidth(double width) {
    double scale = width / 4.14;
    return (widthMultiplier * scale);
  }

  static double scaleHeight(double height) {
//    double scale = height / 8.7;
    double scale = height / 9.1;
    return (heightMultiplier * scale);
  }
}

//class SizeConfig {
//  static MediaQueryData _mediaQueryData;
//  static double screenWidth;
//  static double screenHeight;
//  static double blockSizeHorizontal;
//  static double blockSizeVertical;
//
//  static double _safeAreaHorizontal;
//  static double _safeAreaVertical;
//  static double safeBlockHorizontal;
//  static double safeBlockVertical;
//
//  void init(BuildContext context) {
//    _mediaQueryData = MediaQuery.of(context);
//    screenWidth = _mediaQueryData.size.width;
//    screenHeight = _mediaQueryData.size.height;
//    blockSizeHorizontal = screenWidth / 100;
//    blockSizeVertical = screenHeight / 100;
//
//    _safeAreaHorizontal =
//        _mediaQueryData.padding.left + _mediaQueryData.padding.right;
//    _safeAreaVertical =
//        _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;
//    safeBlockHorizontal = (screenWidth - _safeAreaHorizontal) / 100;
//    safeBlockVertical = (screenHeight - _safeAreaVertical) / 100;
//  }
//
//  static double scaleTextFont(double fontSize) {
//    double scale = fontSize / 4.14;
//    return ((safeBlockHorizontal * scale));
//  }
//
//  static double scaleWidth(double width) {
//    double scale = width / 4.14;
//    return ((blockSizeHorizontal * scale));
//  }
//
//  static double scaleHeight(double height) {
////    double scale = height / 8.7;
//    double scale = height / 9.0;
//    return ((blockSizeVertical * scale));
//  }
//}
